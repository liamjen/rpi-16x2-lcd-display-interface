import json, os, requests

f = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys.darksky'))
DARKSKY_KEY = str(f.readline()).rstrip('\n')
COORDS = str(f.readline()).rstrip('\n')
f.close()
DARKSKY_API_URL = 'https://api.darksky.net/forecast/'
FULL_API_URL = DARKSKY_API_URL + DARKSKY_KEY + "/" + COORDS

class DarkSkyAPI:
    def get_current_weather():
        """
        returns dictionary of current weather
        key = weather attribute : value = weather attribute value

        dict keys:

        temperature
        time
        summary
        visibility
        nearestStormBearing
        precipProbability
        windSpeed
        cloudCover
        ozone
        icon
        dewPoint
        pressure
        apparentTemperature
        uvIndex
        windGust
        precipIntensity
        nearestStormDistance
        windBearing
        humidity
        """
        r = requests.get(FULL_API_URL)
        # throws exception if response code is not 200
        r.raise_for_status()
        weather = {}
        current_weather_json = r.json()['currently']
        for i in current_weather_json:
            weather[i] = current_weather_json[i]
        return weather

