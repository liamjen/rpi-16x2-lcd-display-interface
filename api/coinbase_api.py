import json, hmac, hashlib, time, requests
import os.path
from requests.auth import AuthBase

f = open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'keys.cb'))
CB_API_KEY = str(f.readline()).rstrip('\n')
CB_API_SECRET = str(f.readline()).rstrip('\n')
f.close()
CB_API_URL = 'https://api.coinbase.com/v2/'

EXCHANGE_API_URL = 'https://blockchain.info/ticker'

class CoinbaseAPI:
    """
    update_interval in seconds
    """
    def __init__(self):
        self.auth = self.CoinbaseWalletAuth(CB_API_KEY, CB_API_SECRET)

    def get_btc_total(self):
        r  = requests.get(CB_API_URL + 'accounts', auth=self.auth)
        # throws exception if response code is not 200
        r.raise_for_status()
        return float(r.json()['data'][0]['balance']['amount'])

    def get_btc_to_usd(btc):
        r = requests.get(EXCHANGE_API_URL)
        # throws exception if response code is not 200
        r.raise_for_status()
        return float(r.json()['USD']['sell'] * btc)

    # Create custom authentication for Coinbase API
    class CoinbaseWalletAuth(AuthBase):
        def __init__(self, api_key, secret_key):
            self.api_key = api_key
            self.secret_key = secret_key

        def __call__(self, request):
            timestamp = str(int(time.time()))
            message = timestamp + request.method + request.path_url + (request.body or '')
            signature = hmac.new(bytes(self.secret_key, 'latin-1'), bytes(message, 'latin-1'), hashlib.sha256).hexdigest()

            request.headers.update({
                'CB-ACCESS-SIGN': signature,
                'CB-ACCESS-TIMESTAMP': timestamp,
                'CB-ACCESS-KEY': self.api_key,
                'CB-VERSION': '2017-10-02'
            })
            return request

