import time, datetime
from lcd.bitmap_patterns import Patterns
from lcd.lcd_16x2_GPIO_connect import LCDView, LCDViewManager, LCDGPIOConnection, Alignment
from gpiozero import Button, OutputDevice
from api.coinbase_api import CoinbaseAPI
from api.darksky_api import DarkSkyAPI
from pi.system_info import PiSystemInfo

print("RUNNING AT: " + str(datetime.datetime.now()))

sys_info = PiSystemInfo()

coinbase_api = CoinbaseAPI()
btc_tries = 5
got_btc = False
btc = 0
while btc_tries > 0 and not got_btc:
    try:
        btc = coinbase_api.get_btc_total()
        got_btc = True
    except Exception as e:
        print("Attempted to get btc total, trying " + str(btc_tries) + " more times\n" + str(e) + "\n")
        btc_tries -= 1
        time.sleep(3)
"""
Pin configuration

LCD : RPI
4 : 14 Register select
5 : 15 Read/write
6 : 18 Enable
7 : 23 data 0
8 : 24 data 1
9 : 25 data 2
10 : 8 data 3
11 : 7 data 4
12 : 12 data 5
13 : 16 data 6
14 : 20 data 7
"""

connection = LCDGPIOConnection(14, 15, 18, [23, 24, 25, 8, 7, 12, 16, 20])
mem_view = LCDView(connection, alignment=Alignment.CENTER)
btc_view = LCDView(connection)
ip_view = LCDView(connection, alignment=Alignment.CENTER)
time_view = LCDView(connection, alignment=Alignment.CENTER)
weather_view = LCDView(connection, alignment=Alignment.CENTER)

#mem view
mem_view.add_custom_character(Patterns.degrees_c)
mem_view.set_line_update_functions(line0=lambda: ["CPU " + str(PiSystemInfo.get_cpu_temp()), mem_view.custom_char_at(0)],
                                   line1=lambda: ["RAM free: " + str(PiSystemInfo.get_mem_free_percentage() * 100)[0:4] + "%"])

# btc view
btc_view.add_custom_character(Patterns.btc)
btc_view.set_line_update_functions(line0=lambda: [btc_view.custom_char_at(0), str(btc)],
                                   line1=lambda: ["$" + str(CoinbaseAPI.get_btc_to_usd(btc))])

# ip view
ip_view.add_custom_character(Patterns.wifi)
ip_addr = PiSystemInfo.get_local_ip_address()
ip_view.set_line_update_functions(line0=lambda: [ip_view.custom_char_at(0), str(ip_addr)])

# time view
time_view.set_line_update_functions(line0=lambda: [str(datetime.datetime.now().time()).split('.')[0]])
microsecond_to_second = lambda x: x / 1000000

# weather view
weather_view.add_custom_character(Patterns.degrees_f)
weather_view.add_custom_character(Patterns.uv)
weather_view.add_custom_character(Patterns.wind)
weather_view.add_custom_character(Patterns.miles_hr)
weather_view.add_custom_character(Patterns.humidity)

class WeatherUpdateFunctions:
    """
    line0 and line1 must share weather objects to minimize api calls
    """
    def __init__(self, api):
        self._weather = None
        self._api = api

    def line0(self):
        self._weather = self._api.get_current_weather()
        return [str(int(self._weather['apparentTemperature'])), weather_view.custom_char_at(0), ' ', weather_view.custom_char_at(2), str(int(self._weather['windSpeed'])), weather_view.custom_char_at(3), ' ' + str(self._weather['uvIndex']), weather_view.custom_char_at(1), ' ' + str(int(self._weather['humidity'] * 100)), weather_view.custom_char_at(4)]

    def line1(self):
        return [str(self._weather['summary'].replace('and ', ''))]

weather_funcs = WeatherUpdateFunctions(DarkSkyAPI)
weather_view.set_line_update_functions(line0=weather_funcs.line0,
                                       line1=weather_funcs.line1)

output = OutputDevice(19, initial_value=True)
button = Button(26, pull_up=False)

manager = LCDViewManager()
manager.add_views([mem_view, ip_view], update_interval=10)
manager.add_view(btc_view, update_interval=120, update_on_switch=False)
manager.add_view(weather_view, update_interval=300, update_on_switch=False)
manager.add_view(time_view, update_interval_function=(lambda:.1+ microsecond_to_second(datetime.datetime.now().microsecond)))

button.when_pressed = manager.display_next_view
manager.display_next_view()

while True:
    time.sleep(100)

