from gpiozero import OutputDevice
from enum import Enum
from threading import Thread, Lock
import threading
import time

"""
classes:

LCDGPIOConnection - handles pin on/off for interfacing with 16x2 LCD (no i2c)

Alignment - alignment justification for text on LCD display (left, center, right)

LCDView - class used to abstract writing to 16x2 displays and custom characters. Uses LCDGPIOConnection in backend.
	Inner classes:
	DisplayableOnLCD - Interface for any type of displayable character on LCD
	DisplayableString - class implements DisplayableOnLCD
	DisplayableCustomCharacter - class implements DisplayableOnCLD
	CustomCharacter - data class for holding a 5x7 bitmask for a custom character (array of 7 5-character strings)

LCDViewManager - handles mutliple views on a single LCD display. Automatic updates on set intervals using threads.
"""

LCD_WIDTH = 16
SECOND_LINE_SHIFT_AMOUNT = 24

class LCDGPIOConnection:
    """
    class dealing with sending commands to 16x2 LCD character display

    reg_select_pin = pin number of register select pin
    read_write_pin = pin number of read/write pin
    enable_pin     = pin number of enable pin
    data_pins      = array of 8 integers, datapins 0 - 7
    """
    # set data outs to physical pins
    # default all to 0 (low)
    def __init__(self, reg_select_pin, read_write_pin, enable_pin, data_pins):
        self._reg_select = OutputDevice(reg_select_pin, initial_value=False)
        self._read_write = OutputDevice(read_write_pin, initial_value=False)
        self._enable =     OutputDevice(enable_pin, initial_value=False)
        if len(data_pins) is 8:
            self._data_pins = [
                OutputDevice(data_pins[0], initial_value=False),
                OutputDevice(data_pins[1], initial_value=False),
                OutputDevice(data_pins[2], initial_value=False),
                OutputDevice(data_pins[3], initial_value=False),
                OutputDevice(data_pins[4], initial_value=False),
                OutputDevice(data_pins[5], initial_value=False),
                OutputDevice(data_pins[6], initial_value=False),
                OutputDevice(data_pins[7], initial_value=False)
                ]
        else:
            raise ValueError('data_pins length must be 8, length is: ' + str(len(data_pins)))

        self._initialize_display()


    def write_character(self, char, ascii_index=-1):
        """
        Writes 1 character to the display
        using ascii value bit shifted to enable data pins

        if ascii index >= 0, that value will be printed
        from the table instead (0-7 reserved for custom characters)
        """
        self._reg_select.on()
        i = 0
        val = 0
        if ascii_index > -1:
            val = ascii_index
        else:
            val = ord(char)
        while val > 0:
            if val & 1:
                self._data_pins[i].on()
            val = val >> 1
            i += 1
        self._enable.on()
        self._enable.off()
        self._reg_select.off()
        self._disable_pins(self._data_pins)

    def write_static_string_to_lines(self, line0, line1):
        """
        Writes a string by writing the first 16 characters
        then shifting until 40 (to second line) to write next
        set of 16 characters. only 32 characters are usable
        from the string

        returns home after writing both strings
        """
        # self._initialize_display()
        line0 = line0[0:LCD_WIDTH]
        for c in line0:
            self.write_character(c)
        for i in range(SECOND_LINE_SHIFT_AMOUNT):
            self._cursor_display_shift()
        line1 = line1[0:LCD_WIDTH]
        for c in line1:
            self.write_character(c)
        self._return_home()

    def _initialize_display(self):
        """
        Ran to clear and reset display upon run
        and after a new string needs to be written
        """
        self._clear_display()
        self._cursor_display_shift()
        self._display_on_off_control()
        self._entry_mode_set()
        self._function_set()
        self._return_home()

    def _function_set(self, data_len=True, num_line=True, font=False):
        """
        data_len 0 = 4 bit 1 = 8 bit
        num_line 0 = 1 line 1 = 2 lines
        font 0 = 5x7 1 = 5x10
        """
        self._data_pins[5].on()
        if data_len:
            self._data_pins[4].on()
        if num_line:
            self._data_pins[3].on()
        if font:
            self._data_pins[2].on()
        self._enable.on()
        self._enable.off()
        self._disable_pins(self._data_pins[0:6])

    def _display_on_off_control(self, display=True, cursor=False, position_blink=False):
        """
        command to control if the display, cursor, and cursor blink is visible
        """
        self._data_pins[3].on()
        if display:
            self._data_pins[2].on()
        if cursor:
            self._data_pins[1].on()
        if position_blink:
            self._data_pins[0].on()
        self._enable.on()
        self._enable.off()
        self._disable_pins(self._data_pins[0:4])

    def _cursor_display_shift(self, screen_move=False, shift_right=True):
        """
        command to set shifts to move cursor or full display, and if the cursor
        or display shifts to the right
        """
        self._data_pins[4].on()
        if screen_move:
            self._data_pins[3].on()
        if shift_right:
            self._data_pins[2].on()
        self._enable.on()
        self._enable.off()
        self._disable_pins(self._data_pins[0:5])

    def _return_home(self):
        """
        returns cursor to the top left position
        lcd display takes extra time to return home
        """
        self._data_pins[1].on()
        self._enable.on()
        self._enable.off()
        time.sleep(0.05)
        self._data_pins[1].off()

    def _clear_display(self):
        """
        removes all characters currently visible on the display
        lcd display takes extra time to clear the display
        """
        self._data_pins[0].on()
        self._enable.on()
        self._enable.off()
        time.sleep(0.05)
        self._data_pins[0].off()

    def _entry_mode_set(self, increment=True, shift=False):
        """
        command to set the cursor shift direction (increment or decrement) and
        display/cursor shift (true for display shift)
        """
        self._data_pins[2].on()
        if increment:
            self._data_pins[1].on()
        if shift:
            self._data_pins[0].on()
        self._enable.on()
        self._enable.off()
        self._disable_pins(self._data_pins[0:3])

    def _enable_pins(self, arr):
        """
        enable all pins in arr
        """
        for i in arr:
            i.on()

    def _disable_pins(self, arr):
        """
        disable all pins in arr
        """
        for i in arr:
            i.off()

    def __str__(self):
        print("Register select pin: " + str(self._reg_select))
        print("Read/Write pin: " + str(self._read_write))
        print("Enable pin: " + str(self._enable))
        for i in self._data_pins:
            print("Data pin " + str(i) + ":" + str(self._data_pins[i]))

    def load_custom_characters(self, custom_characters):
        """
        automatically places custom characters in index 0 - 7 on
        ascii table CG RAM
        """
        self._reg_select.off()
        self._read_write.off()
        self._data_pins[6].on()
        self._enable.on()
        self._enable.off()
        self._reg_select.on()
        for custom_character in custom_characters:
            for byte in custom_character.bytes:
                i = 0
                while byte > 0:
                    if byte & 1:
                        self._data_pins[i].on()
                    i += 1
                    byte = byte >> 1
                self._enable.on()
                self._enable.off()
                self._disable_pins(self._data_pins[0:6])

        self._reg_select.off()
        self._disable_pins(self._data_pins)
        self._read_write.off()
        self._clear_display()


    def write_custom_character(self, index):
        """
        ascii_index is the index to write from the
        ascii table

        0 - 7 is reserved for custom characters
        """
        self.write_character("", ascii_index=index)




class Alignment(Enum):
    """
    Enum for selecting alignment of characters on LCD display
    """
    LEFT = 1
    CENTER = 2
    RIGHT = 3


class LCDView:
    """
    Used to create views to define line 1 and line 2 of the LCD
    display connected. Views can be centered and aligned

    Preferred method to write to LCD display - handles LCD Connection, custom characters,
    and alignment of characters on screen

    line0 and line1 are arrays of DisplyableOnLCD

    custom_char_index is the amount of custom chars loaded
    """
    def __init__(self, connection, alignment=Alignment.LEFT):
        self._connection = connection
        # displayable members holds strings and custom characters for each LCD line
        # 0 : line0
        # 1 : line1
        self._displayable_members = {0 : [],
                                     1 : []}
        self._custom_characters = []
        self._custom_char_index = 0
        self._alignment = alignment
        self._update_functions = {0 : None,
                                  1 : None}

    def dstr(string):
        """
        static function to be used when writing the update functions for the lines.
        use this to build an array of displayables
        """
        return LCDView.DisplayableString(str(string))

    def set_lines(self, line0=None, line1=None, lines=None):
        """
        Set displayable member lines to line0 and line 1
        line0 and line1 must be arrays of DisplayableOnLCD
        line0: line0
        line1: line1
        lines: array of size > 2 to set line0 and line1
        """
        if lines is not None:
            for i in range(0, 2):
                self._displayable_members[i] = list(map(lambda x: LCDView.dstr(x) if isinstance(x, str) else x, lines[i]))
        else:
            if line0 is not None:
                self._displayable_members[0] = list(map(lambda x: LCDView.dstr(x) if isinstance(x, str) else x, line0))
            if line1 is not None:
                self._displayable_members[1] = list(map(lambda x: LCDView.dstr(x) if isinstance(x, str) else x, line0))

    def set_line_update_functions(self, line0=None, line1=None, lines=None):
        """
        The two functions must return an array of string and custom characters
        line0: line0 update function
        line1: line1 update function
        lines: sets both lines in a list of line update functions
        """
        if lines is not None:
            for i in range(0, 2):
                self._update_functions[i] = lines[i]
        else:
            if line0 is not None:
                self._update_functions[0] = line0
            if line1 is not None:
                self._update_functions[1] = line1

    def update_lines(self):
        """
        If an update function was not set for a line the line will remain static
        """
        if self._update_functions[0]:
            self._displayable_members[0] = list(map(lambda x: LCDView.dstr(x) if isinstance(x, str) else x, self._update_functions[0]()))
        if self._update_functions[1]:
            self._displayable_members[1] = list(map(lambda x: LCDView.dstr(x) if isinstance(x, str) else x, self._update_functions[1]()))

    def show_view(self, load_custom_characters=True):
        """
        displays view self

        load_custom_characters will overwrite the current custom characters in CG RAM
        for the custom characters in the self view
        """
        if load_custom_characters:
            self._connection.load_custom_characters(self._custom_characters)

        for line in self._get_padded_lines():
            for displayable in line:
                displayable.display(self._connection)
            for i in range(SECOND_LINE_SHIFT_AMOUNT):
                self._connection._cursor_display_shift()

        self._connection._return_home()

    def add_custom_character(self, bitmap_pattern, index=-1):
        """
        adds a custom character bitmap to the self view

        custom characters will be added to the next location unless
        specified by index
        """
        index = index if index >= 0 else self._custom_char_index
        if self._custom_char_index < 8:
            self._custom_characters.append(self.CustomCharacter(bitmap_pattern, index))
            self._custom_char_index += 1
        else:
            raise ValueError('Too many custom characters. Maximum custom characters is 8')

    def append_custom_character(self, index, line):
        """
        add custom character at index to the end of the current displayable line
        """
        self._displayable_members[line].append(self.DisplayableCustomCharacter(self._custom_characters[index]))

    def prepend_custom_character(self, index, line):
        """
        add custom character at index to the beginning of the current displayable line
        """
        self._displayable_members[line] = [self.DisplayableCustomCharacter(self._custom_characters[index])] + self._displayable_members[line]

    def custom_char_at(self, index):
        """
        returns custom character specified at this index (0 - 7)
        wraps in DisplayableCustomCharacter for building line update function
        """
        return self.DisplayableCustomCharacter(self._custom_characters[index])

    def _get_trimmed_line(line):
        """
        private static helper function to trim lines to max of 16 characters each
        """
        trimmed_line = []
        width = 0
        for displayable in line:
            if width + displayable.size() >= LCD_WIDTH:
                trimmed_line.append(displayable.trim(0, LCD_WIDTH - width))
                break
            else:
                trimmed_line.append(displayable)
            width += displayable.size()
        return trimmed_line

    def _get_padded_lines(self):
        """
        pads lines to justify right, left, or center
        lines returned are of size maximum = 16 to fit on 16 width display
        """
        line0 = LCDView._get_trimmed_line(self._displayable_members[0])
        line1 = LCDView._get_trimmed_line(self._displayable_members[1])

        if self._alignment == Alignment.LEFT:
            return(self._pad_align_left(line0), self._pad_align_left(line1))
        elif self._alignment == Alignment.RIGHT:
            return(self._pad_align_right(line0), self._pad_align_right(line1))
        elif self._alignment == Alignment.CENTER:
            return(self._pad_align_center(line0), self._pad_align_center(line1))

    def _pad_align_left(self, line):
        """
        All alignment methods create a DisplayableString object
        with a string that is all spaces to fill up the rest of
        the 16 character line
        """
        total_length = sum(map(lambda x: x.size(), line))
        padding = " " * (LCD_WIDTH - total_length)
        return line + [self.DisplayableString(padding)]

    def _pad_align_right(self, line):
        """
        All alignment methods create a DisplayableString object
        with a string that is all spaces to fill up the rest of
        the 16 character line
        """
        total_length = sum(map(lambda x: x.size(), line))
        padding = " " * (LCD_WIDTH - total_length)
        return [self.DisplayableString(padding)] + line

    def _pad_align_center(self, line):
        """
        All alignment methods create a DisplayableString object
        with a string that is all spaces to fill up the rest of
        the 16 character line
        """
        total_length = sum(map(lambda x: x.size(), line))
        padding = " " * int((LCD_WIDTH - total_length) / 2)
        displayable_padding = [self.DisplayableString(padding)]
        # in the case of an odd length string, one extra space is needed
        if total_length % 2 == 1:
            return displayable_padding + line + [self.DisplayableString(padding + " ")]
        else:
            return displayable_padding + line + displayable_padding

    class DisplayableOnLCD:
        """
        abstract method for displaying either a custom character
        or a regular string of characters
        """
        def display(self, connection):
            raise NotImplementedError("No display method supplied for: " + str(self))

        def size(self):
            raise NotImplementedError("No size method supplied for: " + str(self))

        def trim(self, start, end):
            raise NotImplementedError("No trim method supplied for: " + str(self))

        def __str__(self):
            return str(self)

    class DisplayableString(DisplayableOnLCD):
        """
        class extends DisplayableCharacter for displaying
        strings on LCD
        """
        def __init__(self, string):
            self._string = string

        def display(self, connection):
            for c in self._string:
                connection.write_character(c)

        def size(self):
            return len(self._string)

        def trim(self, start, end):
            """
            returns a new DisplayableString from index start to index end
            """
            return LCDView.DisplayableString(self._string[start : end])

        def __str__(self):
            return str(self._string)

    class DisplayableCustomCharacter(DisplayableOnLCD):
        """
        class extends DisplayableCharacter for displaying
        a custom character on LCD
        """
        def __init__(self, custom_character):
            self._custom_character = custom_character

        def display(self, connection):
            connection.write_character("", ascii_index=self._custom_character.index)

        def size(self):
            return 1

        def trim(self, start, end):
            if start is end:
                return self


        def __str__(self):
            return "Custom character at index: " + str(self._custom_character.index)


    class CustomCharacter:
        """
        class used to hold bytes for writing
        custom characters

        takes string arrays that look like this:
        arr = [
        '.....',
        '..0..',
        '...0.',
        '00000',
        '...0.',
        '..0..',
        '.....',
        '.....',
        ]

        index: CG RAM on LCD. 0 through 7 reserved
        """
        def __init__(self, str_bitmap, index):
            self.bytes = []
            self.index = index
            for string in str_bitmap:
                current_byte = 0
                mask = 1
                for c in string[::-1]:
                    current_byte = current_byte | mask if c is not '.' else current_byte
                    mask = mask << 1

                self.bytes.append(current_byte)


class LCDViewManager:
    """
    handles viewing of multiple views on LCD Display

    uses threads to update views on a set interval, or displays a static
    view
    """
    def __init__(self):
        self.lock = Lock()
        self.views = []
        self.current_view = 0
        self.lowest_update_interval = 0
        self.last_view_update = {}
        self.threads = []

    def _wait_on_next_update(self):
        """
        if the update thread calling this function is the most recent, the thread will sleep until the set
        interval and update the display if the current view is still the most recent
        """
        while threading.current_thread() is self.threads[-1]:
            self.lock.acquire()
            update_interval_val = self.views[self.current_view][1]
            update_interval_function = self.views[self.current_view][2]
            update_interval = update_interval_val if update_interval_val > 0 else update_interval_function() if update_interval_function is not None else 0

            if update_interval > 0:
                # if current time is over the refresh interval
                current_time = time.time()
                last_update_time_difference = current_time - self.last_view_update[self.current_view]
                if (last_update_time_difference) > update_interval:
                    self.views[self.current_view][0].update_lines()
                    self.views[self.current_view][0].show_view(load_custom_characters=False)
                    self.last_view_update[self.current_view] = time.time()
                    self.lock.release()
                else:
                    self.lock.release()
                    sleep_time = update_interval - last_update_time_difference
                    time.sleep(sleep_time)
            else:
                # static view will be stopped within 10 seconds after shifting
                self.lock.release()
                time.sleep(10)


    def add_view(self, view, update_interval=0, update_interval_function=None, update_on_switch=True):
        """
        views with update_interval > 0 will be refreshed on
        screen at that interval (seconds) if it is currently being
        displayed

        update_interval_function to be used to specify the amount of time to
        wait in seconds with a function that returns a num

        tuple object is the view, update interval, update_interval_function, update_on_switch

        update_on_switch will call view update functions when the view is selected if True else will wait
        for the update interval if not as much time as the update interval has passed
        """
        if update_interval < 0:
            raise ValueError('update interval must be >= 0 : ' + str(update_interval))

        tup = (view, update_interval, update_interval_function, update_on_switch)
        self.last_view_update[len(self.views)] = 0
        self.views = self.views + [tup]
        if update_interval < self.lowest_update_interval:
            self.lowest_update_interval = update_interval
        elif update_interval > 0 and self.lowest_update_interval is 0:
            self.lowest_update_interval = update_interval


    def add_views(self, views, update_interval=0, update_interval_function=None, update_on_switch=True):
        """
        use to add multiple views with the same update interval, function, and switch setting
        """
        for i in views:
            self.add_view(i, update_interval, update_interval_function, update_on_switch)

    def display_next_view(self):
        """
        displays the next view in the manager on the LCD display.

        if the view has update_on_switch flag to true the view will update the content of
        the lines right after the switch
        """
        self.lock.acquire()
        self.current_view += 1
        if self.current_view >= len(self.views):
            self.current_view = 0

        # if update_on_switch
        if self.views[self.current_view][3]:
            self.views[self.current_view][0].update_lines()
            self.last_view_update[self.current_view] = time.time()

        self.views[self.current_view][0].show_view()
        self.threads = list(filter(lambda x: x.is_alive(), self.threads))
        self.threads.append(Thread(target=self._wait_on_next_update))
        self.threads[-1].start()
        self.lock.release()


