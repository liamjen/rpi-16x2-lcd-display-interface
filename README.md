# RPi 16x2 LCD Display Interface

Classes to interface with a standard 16x2 LCD Display using a Raspberry Pi's GPIO pins.

Requires: requests, gpiozero

![](https://imgur.com/F9y5Egs.jpg)
![](https://imgur.com/N3Z4Jit.jpg)
![](https://imgur.com/hoUbS9u.jpg)
