import socket

_cpu_temp_file = '/sys/class/thermal/thermal_zone0/temp'
_mem_stat_file = '/proc/meminfo'
# uptime in seconds
_uptime_stat_file = '/proc/uptime'

class PiSystemInfo:

    # cpu temp in c
    def get_cpu_temp():
        f = open(_cpu_temp_file)
        temp = int(f.readline()) / 1000
        f.close()
        return temp

    def get_mem_free_percentage():
        f = open(_mem_stat_file)
        mem_total = int(f.readline().split(' ')[-2:-1][0])
        f.readline()
        mem_available = int(f.readline().split(' ')[-2:-1][0])
        f.close()
        return float(mem_available / mem_total)

    def get_local_ip_address():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            # doesn't even have to be reachable
            s.connect(('10.255.255.255', 1))
            IP = s.getsockname()[0]
        except:
            IP = '127.0.0.1'
        finally:
            s.close()
        return IP


